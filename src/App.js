import React from 'react';
import './App.css';
// import gear from '../public/img/gear.svg';


class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: {}
    };
  }

  componentDidMount() {
  fetch("http://jgw4r.mocklab.io/thing/8")
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items: result
        });
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
}

 render() {
   var data = this.state
   // console.log(data)
   // console.log(data.currentTime);
   // var tempTime = new Date()
   // console.log(tempTime);
   // var time = tempTime.getFullYear()
   var hour = new Date().getHours()
   var minutes = new Date().getMinutes()
   // console.log(hour + minutes)
   var date = new Date().getDate()
   var month = new Date().getMonth() + 1
   var year = new Date().getFullYear()

  return (
    <div className="App">
      <div className="left">
      <img src="img/logo.svg" alt="Logo" className="logo" />
        <ul className="navList">
          <li className="currentTab">
            <img src="img/dashboard.svg" className="icon" alt="Dashboard" />
          </li>
          <li className="tab">
            <img src="img/map.svg" className="icon" alt="Map" />
          </li>
          <li className="tab">
            <img src="img/settings.svg" className="icon" alt="Dashboard" />
          </li>
        </ul>
      </div>
      <div className="right">
        <div className="row1">
          <div className="row1-table">
            <div className="row1-table-cell row1-table-cell-left">
              <h2 className="time">{hour}:{minutes}</h2>
              <h3 className="date">{date}/{month}/{year}</h3>
            </div>
            <div className="row1-table-cell row1-table-cell-right">
              <ul className="weatherList">
                <li className="weatherListItem">
                  Temperature: 26C
                </li>
                <li className="weatherListItem">
                  Humidity: {data.items.humidity}%
                </li>
                <li className="weatherListItem">
                  Wind: {data.items.wind} km/H
                </li>
                <li className="weatherListItem">
                  Pressure: {data.items.pressure} hPa
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="row2">
          <div className="grid">
            <div className="grid-col grid-col-1">
              <div className="speed">
                <div className="speed-text">
                  <p className="speed-top">Speed</p>
                  <h3 className="speed-number">{data.items.kmPerHour}</h3>
                  <p className="speed-bottom">Km/H</p>
                </div>
              </div>
            </div>
            <div className="grid-col grid-col-2">
              <img src="img/playing.png" className="chart" alt="Playing" />
              <img src="img/progress.svg" className="chart" alt="Progress" />

              <div className="currently-info-wrap">
                <img src="img/shuffle.svg" className="shuffle" alt="Shuffle" />
                <p className="currenlty-time">{data.items.currentTime}</p>
              </div>
            </div>
            <div className="grid-col grid-col-3">
            <div className="gear">
              <div className="gear-text">
                <p className="gear-top">Gear</p>
                <h3 className="gear-number">{data.items.gear}</h3>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div className="row3">
        <div className="row3-list-wrap">
          <ul className="infoList">
            <li className="infoListItem">
              <img src="img/energy.svg" className="mono-icon" alt="Bolt" />
              <p className="list-mono">Battery: {data.items.energy}%</p>
            </li>
            <li className="weatherListItem">
              <img src="img/fluid.svg" className="mono-icon" alt="Bolt" />
              <p className="list-mono">Break Fluid: {data.items.breakFluid}%</p>
            </li>
            <li className="weatherListItem">
              <img src="img/tire.svg" className="mono-icon" alt="Bolt" />
              <p className="list-mono">Tire Wear: {data.items.tireWear}%</p>
            </li>
            <li className="weatherListItem">
              <img src="img/range.svg" className="mono-icon" alt="Bolt" />
              <p className="list-mono">Range: {data.items.range}Km</p>
            </li>
            <li className="weatherListItem">
              <img src="img/totalkm.svg" className="mono-icon" alt="Bolt" />
              <p className="list-mono">Total Km: {data.items.overallKm}Km</p>
            </li>
          </ul>
        </div>

          </div>
      </div>
    </div>
  );
}
}
export default App;
